import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as SecureStore from "expo-secure-store";
import HomeScreen from './pages/HomeScreen.js';
import AddPlantScreen from './pages/AddPlantScreen.js';
import LoginScreen from './pages/Login.js';
import SignUpScreen from './pages/SignUpScreen.js';
import ForgotPasswordScreen from './pages/ForgotPasswordScreen.js';
import MonJardinScreen from './pages/MonJardinScreen.js';
import GalleriePage from './pages/GalleriePage.js';
import Profil from "./pages/Profil";
import Map from "./pages/Map";
import EditProfil from "./pages/EditProfil";
import LoadingScreen from './pages/LoadingScreen';
import {refreshToken} from "./lib/fetch/auth";
import Chat from "./pages/Chat";

const Stack = createStackNavigator();

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(null);

  useEffect(() => {
    const checkLoginStatus = async () => {
      try {
        const response = await refreshToken();
        if (response.success) {
            SecureStore.setItem('loginStatus', 'true');
        } else {
            SecureStore.setItem('loginStatus', 'false');
        }
        const value = SecureStore.getItem('loginStatus');
        setIsLoggedIn(value === 'true');
      } catch(e) {
        // error reading value
        setIsLoggedIn(false);
      }
    };

    checkLoginStatus();
  }, []);

  if (isLoggedIn === null) {
    return <LoadingScreen />;
  }

  return (
      <NavigationContainer>
        {/*<Stack.Navigator>*/}
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{title: 'Se connecter' } }/>
          <Stack.Screen name="SignUp" component={SignUpScreen} options={{title: 'Inscription'}}/>
          <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={{title : 'Mot de passe oublié'}}/>
          <Stack.Screen name="Chat" component={Chat} options={{title : 'Acces rapide chat'}}/>
    {isLoggedIn ? (
        <Stack.Navigator>
    {/*/!*app*!/*/}
          <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}} />
          <Stack.Screen name="AddPlant" component={AddPlantScreen} options={{ title: 'Ajouter une Plante' }} />
          <Stack.Screen name="MonJardinScreen" component= {MonJardinScreen} options= {{title: 'Mon jardin'}}/>
          <Stack.Screen name="GalleriePage" component= {GalleriePage} options= {{title: 'Gallerie'}}/>
          <Stack.Screen name="Map" component= {Map} options= {{title: 'Carte des Plantes'}}/>
          <Stack.Screen name="Profil" component= {Profil} options= {{title: 'Profil'}}/>
          <Stack.Screen name="EditProfil" component= {EditProfil} options= {{title: 'Modifier le profil'}}/>
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{title: 'Se connecter' } }/>
          <Stack.Screen name="SignUp" component={SignUpScreen} options={{title: 'Inscription'}}/>
          <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={{title : 'Mot de passe oublié'}}/>
        </Stack.Navigator>
    ) : (
        <Stack.Navigator>
          {/* authentication */}
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{title: 'Se connecter', isLoggedIn: isLoggedIn } }/>
          <Stack.Screen name="SignUp" component={SignUpScreen} options={{title: 'Inscription'}}/>
          <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={{title : 'Mot de passe oublié'}}/>
          <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}} />
          <Stack.Screen name="AddPlant" component={AddPlantScreen} options={{ title: 'Ajouter une Plante' }} />
          <Stack.Screen name="MonJardinScreen" component= {MonJardinScreen} options= {{title: 'Mon jardin'}}/>
          <Stack.Screen name="GalleriePage" component= {GalleriePage} options= {{title: 'Gallerie'}}/>
          <Stack.Screen name="Map" component= {Map} options= {{title: 'Carte des Plantes'}}/>
          <Stack.Screen name="Profil" component= {Profil} options= {{title: 'Profil'}}/>
          <Stack.Screen name="EditProfil" component= {EditProfil} options= {{title: 'Modifier le profil'}}/>
        </Stack.Navigator>
    )}
  {/*</Stack.Navigator>*/}
</NavigationContainer>
  );
};

export default App;