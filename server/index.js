const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const mysql = require('mysql2');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

app.use(express.json());

const db = mysql.createConnection({
    host: 'localhost',
    user: 'your-username',
    password: 'your-password',
    database: 'chat_db'
});

db.connect(err => {
    if (err) throw err;
    console.log('MySQL connected...');
});

let users = {};

io.on('connection', (socket) => {
    console.log('Utilisateur connecté');

    socket.on('join', (username) => {
        users[username] = socket.id;
        socket.username = username;
        console.log(`${username} has joined with socket id ${socket.id}`);
    });

    socket.on('private_message', async ({ recipient, message }) => {
        const recipientSocketId = users[recipient];
        if (recipientSocketId) {
            io.to(recipientSocketId).emit('private_message', {
                sender: socket.username,
                message
            });
        }

        // Rechercher les IDs des utilisateurs
        const senderQuery = 'SELECT id FROM users WHERE username = ?';
        const recipientQuery = 'SELECT id FROM users WHERE username = ?';

        db.query(senderQuery, [socket.username], (err, senderResult) => {
            if (err) throw err;
            db.query(recipientQuery, [recipient], (err, recipientResult) => {
                if (err) throw err;
                const senderId = senderResult[0].id;
                const recipientId = recipientResult[0].id;
                const query = 'INSERT INTO messages (sender_id, recipient_id, message) VALUES (?, ?, ?)';
                db.query(query, [senderId, recipientId, message], (err, result) => {
                    if (err) throw err;
                    console.log('Message stored in DB');
                });
            });
        });
    });

    socket.on('disconnect', () => {
        delete users[socket.username];
        console.log(`${socket.username} est deconnecté`);
    });
});

const PORT = process.env.PORT || 5000;
server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
