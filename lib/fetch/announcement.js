import Constants from 'expo-constants';
import {storeTokens, tokenIsValid} from "../utilFunctions";
import {refreshToken} from "./auth";
import * as SecureStore from "expo-secure-store";

const apiUrl = Constants.expoConfig.extra.API_URL;

const fetchAnnouncementByUser = async (userId) => {
    return await fetch(`${apiUrl}/announcements?user=${userId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
    .then(response => response.json())
    .then(data => storeTokens(data));
};

const fetchAnnouncement = async (announcementId) => {
    return await fetch(`${apiUrl}/announcements?id=${announcementId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const fetchAllAnnouncements = async (token) => {
    return await fetch(`${apiUrl}/announcements`,{
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const createAnnouncement = async (token, date_debut, date_fin, periodicite_entretien, image, nom_plante, description, proprietaire_id) => {
    return await fetch(`${apiUrl}/announcements`,{
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }, body: {
            date_debut: date_debut,
            date_fin: date_fin,
            periodicite_entretien: periodicite_entretien,
            image: image,
            nom_plante: nom_plante,
            description: description,
            proprietaire_id: proprietaire_id
        }
    })
};

const editAnnouncement = async (token, announcement, date_debut, date_fin, periodicite_entretien, gardien_id) => {
    return await fetch(`${apiUrl}/announcements?id=${announcement.id}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }, body: {
            date_debut: date_debut || announcement.date_debut,
            date_fin: date_fin || announcement.date_fin,
            periodicite_entretien: periodicite_entretien || announcement.periodicite_entretien,
            gardien_id: gardien_id || announcement.gardien_id,
        }
    })
};

const deleteAnnouncement = async (token, announcementId) => {
    return await fetch(`${apiUrl}/announcements?id=${announcementId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
};

const fetchAnnouncementAddressCoords = async (address) => {
    console.log(address)
    return await fetch(`https://nominatim.openstreetmap.org/search?format=json&q=${encodeURIComponent(address)}`)
        .then(response => response.json())
        .then(data => {
            if (data && data.length > 0) {
                return {
                    latitude: parseFloat(data[0].lat),
                    longitude: parseFloat(data[0].lon),
                }
            } else {
                return "Adresse non trouvée. Veuillez entrer une adresse valide.";
            }
        })
        .catch(error => {
            return "Erreur de géocodage :";
        });
};

export {
    fetchAnnouncementByUser,
    fetchAnnouncement,
    fetchAllAnnouncements,
    createAnnouncement,
    editAnnouncement,
    fetchAnnouncementAddressCoords
};