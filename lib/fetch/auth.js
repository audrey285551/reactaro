import * as SecureStore from "expo-secure-store";
import {deleteTokens, storeTokens} from "../utilFunctions";
import Constants from "expo-constants";

const apiUrl = Constants.expoConfig.extra.API_URL;

const login = async (email, password) => {
    const response = await fetch(`${apiUrl}/auth/signin`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            login: email,
            password: password
        }),
    });
    const data = await response.json();
    return storeTokens(data);
};

const refreshToken = async () => {
    const response = await fetch(`${apiUrl}/auth/refresh`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    });
    return storeTokens(await response.json());
};

const logout = async () => {
    await fetch(`${apiUrl}/auth/logout`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Content-Type': 'application/json',
        }
    });
    deleteTokens();
};

export { login, refreshToken, logout }