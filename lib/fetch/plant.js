import Constants from 'expo-constants';
import {storeTokens, tokenIsValid} from "../utilFunctions";
import * as SecureStore from "expo-secure-store";

const apiUrl = Constants.expoConfig.extra.API_URL;

const fetchSearchPlant = async (token, plantName) => {
    return await fetch(`${apiUrl}/plants?name=${plantName}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

export {
    fetchSearchPlant
};