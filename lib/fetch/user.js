import Constants from 'expo-constants';
import {storeTokens, tokenIsValid} from "../utilFunctions";
import * as SecureStore from "expo-secure-store";

const apiUrl = Constants.expoConfig.extra.API_URL;

const fetchGenders = async () => {
    return await fetch(`${apiUrl}/genders`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => data);
};

const fetchUserData = async (userId) => {
    return await fetch(`${apiUrl}/users?id=${userId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const createUser = async (pseudo, mail, password, rue, ville, code_postal, gender, isBotanist) => {
    return await fetch(`${apiUrl}/auth/signup`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            pseudo: pseudo,
            mail: mail,
            mot_de_passe: password,
            rue: rue,
            ville: ville,
            code_postal: code_postal,
            genre: gender,
            est_botaniste: isBotanist,
            est_admin: false,
            image: '',
        })
    })
};

const editUser = async (token, userData, pseudo, mail, rue, ville, code_postal, gender) => {
    console.log(token)
    console.log(userData)
    return await fetch(`${apiUrl}/users?id=${userData.id}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            pseudo: pseudo || userData.pseudo,
            mail: mail || userData.mail,
            est_botaniste: userData.est_botaniste,
            est_admin: userData.est_admin,
            genre: gender || userData.genre_id,
            image: '' || userData.image_id,
            ville: ville || userData.Adresse.Ville.nom_ville,
            code_postal: code_postal || userData.Adresse.CodePostal.code_postal,
            rue: rue || userData.Adresse.rue,
        })
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const deleteUser = async (token, userId) => {
    return await fetch(`${apiUrl}/users?id=${userId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
};

export {
    fetchGenders,
    fetchUserData,
    createUser,
    editUser,
    deleteUser
};