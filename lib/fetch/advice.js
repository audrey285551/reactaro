import Constants from 'expo-constants';
import {storeTokens, tokenIsValid} from "../utilFunctions";
import {refreshToken} from "./auth";
import * as SecureStore from "expo-secure-store";

const apiUrl = Constants.expoConfig.extra.API_URL;

const fetchAdvice = async (token, refreshToken, adviceId) => {
    return await fetch(`${apiUrl}/advices?id=${adviceId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const fetchAdviceByPlant = async (token, plantId) => {
    return await fetch(`${apiUrl}/advices?plant=${plantId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(data => storeTokens(data));
};

const createAdvice = async (token, content, plantId, botanistId) => {
    return await fetch(`${apiUrl}/advices`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            contenu: content,
            plante_id: plantId,
            botaniste_id: botanistId
        })
    });
};

const editAdvice = async (token, content, adviceId) => {
    return await fetch(`${apiUrl}/advices?id=${adviceId}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            contenu: content
        })
    });
};

const deleteAdvice = async (token, adviceId) => {
    return await fetch(`${apiUrl}/advices?id=${adviceId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${SecureStore.getItem('token')}`,
            'Refresh-Token': SecureStore.getItem('refreshToken'),
            'Content-Type': 'application/json',
        }
    });
};

export {
    createAdvice,
    editAdvice,
    fetchAdvice,
    fetchAdviceByPlant,
    deleteAdvice
};