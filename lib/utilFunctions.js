import * as SecureStore from "expo-secure-store";
import {Buffer} from "buffer";
import dayjs from "dayjs";

const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

const decodeToken = (token) => {
    const parts = token.split('.').map(part => Buffer.from(part.replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString());
    return JSON.parse(parts[1]);
}

const tokenIsValid = (token) => {
    const payload = decodeToken(token);
    return (payload.exp - (Date.now() / 1000)) > 60 * 15;
};

const formatDate = (date) => {
    return dayjs(date).format("DD/MM/YYYY à HH:mm");
};

const storeTokens = (data) => {
    if (data.success) {
        SecureStore.setItem('token', data.token);
        SecureStore.setItem('refreshToken', data.refreshToken);
        const payload = decodeToken(data.token);
        SecureStore.setItem('userId', payload.id.toString());
        SecureStore.setItem('userId', payload.id.toString());
        SecureStore.setItem('username', payload.pseudo);
        return data;
    } else {
        return data;
    }
}

const deleteTokens = () => {
    SecureStore.setItem('token', '');
    SecureStore.setItem('refreshToken', '');
    SecureStore.setItem('userId', '');
}

export {decodeToken, tokenIsValid, capitalize, formatDate, storeTokens, deleteTokens};