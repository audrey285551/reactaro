import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';
import * as SecureStore from 'expo-secure-store';

const socket = io('http://localhost:5000');

const Chat = () => {
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const [recipient, setRecipient] = useState('');
    const [username, setUsername] = useState('');

    useEffect(() => {
        const fetchUsername = async () => {
            const storedUsername = await SecureStore.getItemAsync('username');
            if (storedUsername) {
                setUsername(storedUsername);
                socket.emit('join', storedUsername);
            }
        };
        fetchUsername();

        socket.on('private_message', ({ sender, message }) => {
            setMessages((prevMessages) => [...prevMessages, { sender, message }]);
        });

        return () => {
            socket.off('private_message');
        };
    }, []);

    const sendMessage = () => {
        socket.emit('private_message', { recipient, message });
        setMessages((prevMessages) => [...prevMessages, { sender: 'Me', message }]);
        setMessage('');
    };

    return (
        <div>
            <div>
                {messages.map((msg, index) => (
                    <div key={index}>
                        <strong>{msg.sender}:</strong> {msg.message}
                    </div>
                ))}
            </div>
            <input
                type="text"
                placeholder="Recipient"
                value={recipient}
                onChange={(e) => setRecipient(e.target.value)}
            />
            <input
                type="text"
                placeholder="Message"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
            />
            <button onClick={sendMessage}>Send</button>
        </div>
    );
};

export default Chat;
