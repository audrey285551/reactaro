import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import {logout} from "../lib/fetch/auth";
import * as SecureStore from "expo-secure-store";

const HomeScreen = ({ navigation }) => {
  const logoutAndResetNavigation = async () => {
    await logout();
    SecureStore.setItem('loginStatus', 'false');
    navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            { name: 'LoginScreen' },
          ],
        })
    );
  };

  return (
    <ImageBackground source={require('../images/plante.jpg')} style={styles.background}>
      <View style={styles.container}>
        <Text style={styles.title}>Bienvenue sur Arosa-je</Text>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Profil')}>
          <Text style={styles.buttonText}>Profil</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('MonJardinScreen')}>
          <Text style={styles.buttonText}>Accéder à Mon Jardin</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('GalleriePage')}>
          <Text style={styles.buttonText}>Gallerie</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Map')}>
          <Text style={styles.buttonText}>Carte des Plantes</Text>
        </TouchableOpacity>
        {/*<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('LoginScreen')}>*/}
        {/*  <Text style={styles.buttonText}>Se connecter</Text>*/}
        {/*</TouchableOpacity>*/}
        {/*<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('SignUp')}>*/}
        {/*  <Text style={styles.buttonText}>S'inscrire</Text>*/}
        {/*</TouchableOpacity>*/}
        <TouchableOpacity style={styles.button} onPress={logoutAndResetNavigation}>
          <Text style={styles.buttonText}>Déconnexion</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 34,
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 80,
    textAlign: 'center',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowRadius: 5,
    textShadowOffset: { width: 1, height: 1 },
    position: 'absolute',
    top: 80,
    left: 0,
    right: 0,
    zIndex: 1,
  },
  button: {
    backgroundColor: '#00a65a',
    borderRadius: 5,
    padding: 10,
    width: '80%',
    marginBottom: 10,
    opacity: 0.55,
  },
  buttonText: {
    color: '#ffffff',
    textAlign: 'center',
    fontWeight: 'bold'
  },
});


export default HomeScreen;
