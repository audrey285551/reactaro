import React, {useEffect, useState} from 'react';
import MapView, {Marker} from 'react-native-maps';
import {ActivityIndicator, SafeAreaView, StyleSheet, View} from 'react-native';
import * as SecureStore from "expo-secure-store";
import {fetchAllAnnouncements, fetchAnnouncementAddressCoords} from "../lib/fetch/announcement";


const Map = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [userId, setUserId] = useState(SecureStore.getItem('userId'));
  const [announcements, setAnnouncements] = useState([{}]);
  const [markers, setMarkers] = useState([]);

  const fetchData = async () => {
    try {
      const token = SecureStore.getItem('token');
      let data = await fetchAllAnnouncements(token);
      if (!data) {
        data = await fetchAllAnnouncements(token);
      }
      setAnnouncements(data.annonces);
      if (data.annonces) {
          const markerPromises = data.annonces.map(async (item) => {
              console.log(item)
              const coords = await fetchAnnouncementAddressCoords(
                  `${item.Proprietaire.Adresse.rue} ${item.Proprietaire.Adresse.Ville.nom_ville}`
              );
              return {
                  title: item.Plante.nom_plante,
                  description: item.Plante.description,
                  announcement_id: item.id,
                  start_date: item.date_debut,
                  end_date: item.date_fin,
                  latlng: coords,
              };
          });

          const markers = await Promise.all(markerPromises);
          setMarkers(markers);
      }
    } catch (e) {
      console.log(e)
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <SafeAreaView  style={styles.container}>
      <MapView
          style={styles.map}
          initialRegion={{
            latitude: 43.625050,
            longitude: 3.862038,
            latitudeDelta: 0.15,
            longitudeDelta: 0.15,
          }}
      >
        <>
            {markers.map((marker, index) => {
                console.log('marker :', marker)
                return (<Marker
                    key={index}
                    coordinate={marker.latlng}
                />)
            })}
        </>
      </MapView>
    </SafeAreaView >
  );
}

export default Map;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
});

