import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { login } from "../lib/fetch/auth";
import * as SecureStore from "expo-secure-store";
import { deleteTokens } from "../lib/utilFunctions";
import Chat from "./Chat";

const LoginScreen = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [emailInputStyle, setEmailInputStyle] = useState(styles.input);
  const [isButtonDisabled, setButtonDisabled] = useState(false);

  const validateEmail = (email) => {
    const regex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    return regex.test(email);
  };

  const handleOnChangeEmail = (email) => {
    setEmail(email);
    if (validateEmail(email)) {
      setErrorMessage('');
      setEmailInputStyle(styles.input);
    }
  }

  const handleEmailBlur = () => {
    if (!validateEmail(email)) {
      setErrorMessage('Adresse mail invalide');
      setEmailInputStyle(styles.inputError);
    } else {
      setErrorMessage('');
      setEmailInputStyle(styles.input);
      deleteTokens();
    }
  };

  const handleLogin = async () => {
    setButtonDisabled(true);
    try {
      const response = await login(email, password);
      if (response.success) {
        SecureStore.setItem('loginStatus', 'true');
        navigation.navigate('Home');
      } else {
        setTimeout(function () {
          setErrorMessage(response.message);
          setPassword('');
          setButtonDisabled(false);
        }, 1000);
      }
    } catch (error) {
      console.error(error);
      setButtonDisabled(false);
    } finally {
      setLoading(false);
    }
  }

  return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <Text style={styles.title}>Connexion</Text>
          {errorMessage ? <Text style={styles.error}>{errorMessage}</Text> : null}
          <TextInput
              style={emailInputStyle}
              placeholder="Adresse e-mail"
              keyboardType="email-address"
              autoCapitalize="none"
              value={email}
              onChangeText={handleOnChangeEmail}
              onBlur={handleEmailBlur}
          />
          <TextInput
              style={styles.input}
              placeholder="Mot de passe"
              secureTextEntry={true}
              value={password}
              onChangeText={setPassword}
          />
          <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
            <Text style={styles.forgotPassword}>Mot de passe oublié ?</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} disabled={isButtonDisabled} onPress={handleLogin}>
            <Text style={styles.buttonText}>Se connecter</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text style={styles.signUpLink}>Pas encore inscrit ? S'inscrire</Text>
          </TouchableOpacity>
        </View>
      </View>
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff', // Changer la couleur de fond au besoin
  },
  formContainer: {
    width: '80%',
    alignItems: 'center',
  },
  logo: {
    width: 150, // Ajuster la taille du logo au besoin
    height: 150, // Ajuster la taille du logo au besoin
    marginBottom: 10,
  },
  title: {
    fontSize: 34,
    color: '#000', // Couleur du texte
    marginBottom: 10,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  input: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#ccc', // Couleur de la bordure
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
  },
  forgotPassword: {
    textAlign: 'right',
    marginBottom: 10,
    color: '#007bff', // Couleur du lien "Mot de passe oublié ?"
  },
  button: {
    backgroundColor: '#00a65a',
    borderRadius: 5,
    padding: 10,
    width: '80%',
    marginBottom: 10,
    opacity: 0.55,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  signUpLink: {
    color: '#007bff', // Couleur du lien "Pas encore inscrit ? S'inscrire"
    textAlign: 'center',
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
  inputError: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
  }
});
